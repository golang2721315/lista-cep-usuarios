FROM golang:1.22.2-alpine3.19

WORKDIR /app

COPY . .

RUN GOOS=linux GOARCH=amd64 go build -o main .

FROM alpine:20240329

WORKDIR /app

COPY --from=0 /app/main /app/main
COPY --from=0 /app/template.html /app/template.html

CMD ["./main"]