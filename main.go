package main

import (
	"encoding/json"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
)

var Usuarios struct {
	Nome string
	Cep  struct {
		Cep         string `json:"cep"`
		Logradouro  string `json:"logradouro"`
		Complemento string `json:"complemento"`
		Bairro      string `json:"bairro"`
		Localidade  string `json:"localidade"`
		Uf          string `json:"uf"`
		Ibge        string `json:"ibge"`
		Gia         string `json:"gia"`
		Ddd         string `json:"ddd"`
		Siafi       string `json:"siafi"`
	}
}

func main() {

	if len(os.Getenv("NOME")) == 0 {
		log.Fatal("Variável NOME não foi definida adequadamente")
	}

	if len(os.Getenv("CEP")) == 0 {
		log.Fatal("Variável CEP não foi definida adequadamente")
	}

	if len(os.Getenv("CEP")) != 8 {
		log.Fatal("Variável CEP não possui 8 números")
	}

	if len(os.Getenv("PORTA")) == 0 {
		log.Fatal("Variável PORTA não foi definida adequadamente")
	}

	http.HandleFunc("/", ListaCepUsuariosHandler)
	http.ListenAndServe(":"+os.Getenv("PORTA"), nil)
}

func ListaCepUsuariosHandler(w http.ResponseWriter, r *http.Request) {

	Usuarios.Nome = os.Getenv("NOME")
	Usuarios.Cep.Cep = os.Getenv("CEP")

	req, err := http.Get("https://viacep.com.br/ws/" + Usuarios.Cep.Cep + "/json/")

	if err != nil {
		panic(err)
	}

	defer req.Body.Close()
	res, err := io.ReadAll(req.Body)

	err = json.Unmarshal(res, &Usuarios.Cep)
	if err != nil {
		panic(err)
	}

	t := template.Must(template.New("template.html").ParseFiles("template.html"))
	err = t.Execute(w, Usuarios)
	if err != nil {
		panic(err)
	}
}
